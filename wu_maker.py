#!/usr/bin/python3
import os
import subprocess
import requests
import shutil
import argparse
import yaml
from bs4 import BeautifulSoup
from jinja2 import Environment, FileSystemLoader

# Class for connection and scrapping of HTB
class connection():
    def __init__(self):
        # Login URL, needed to create session
        self.login_url = 'https://www.hackthebox.eu/login'

        # Header to avoid being block as a bot
        self.__headers = { 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0',
                          'Host': 'www.hackthebox.eu',
                          'Referer': 'https://www.hackthebox.eu/login'}

        # Info scrapped on the Challenge page
        self.box_info = { 'box_name': None,
                          'ip': None,
                          'creators': [],
                          'difficulty': None,
                          'os': None}

    # Create HTB session
    def __connect(self):
        # Get user email and password to connect on HTB
        email = input("Email: \n")
        password = input("Password: \n")

        # Initialize session, we need to retrieve the CSRF token
        self.__session = requests.Session()
        page = self.__session.get(self.login_url, headers=self.__headers)
        soup = BeautifulSoup(page.text, 'html.parser')

        # Get the CSRF Token
        token = soup.find('input', {'name': '_token'}).get('value').replace('\n', '')

        # Create form data
        login_form = { '_token': token,
                       'email': email,
                       'password': password}

        # Send data and login
        login_page = self.__session.post(self.login_url, headers=self.__headers, data=login_form)

        if 'These credentials do not match our records.' in login_page:
            print('[-] Cannot connect to HTB. Check your login/password.')
            exit(1)

    # Scrap box page
    def __get_box_info(self, url):
        # Get the box page (url provided in argparse)
        box_page = self.__session.get(url, headers=self.__headers, allow_redirects=False)
        # If a get response
        if box_page.status_code == 200:
            # Convert HTML to BeautifulSoup object
            soup = BeautifulSoup(box_page.text, 'html.parser')
            # Get the name of the box
            self.box_info['box_name'] = soup.find('h3', {'class': 'm-n'}).string.replace('\n', '')
            # Get the ip of the box
            self.box_info['ip'] = soup.findAll('div', {'class': 'col-md-12 col-xs-12 text-center'})[0].find('h2').text.strip()
            # Get the list of creators (might be several of them)
            creators_name = soup.findAll('div', {'class': 'col-md-12 col-xs-12 text-center'})[1].findAll('a')
            for value in creators_name:
                self.box_info['creators'].append({'name': value.string, 'url': value['href']})

            # Select the card modal for last informations
            box_info = soup.find('div', {'id': 'machineCard'}).findAll('tr')
            # Get the box difficulty from the card (easy, medium etc)
            self.box_info['difficulty'] = box_info[1].findAll('td')[1].text.strip()
            # Get the OS of the box from the card
            self.box_info['os'] = box_info[0].findAll('td')[1].text.strip()
            # Return soup object (to download icon)
            return soup
        else:
            print('[-] The challenge page doesn\'t exists.')
            exit(1)

    def download_logo(self, soup, box_name, destination):
        # Get the logo href
        logo = soup.find('img', {'class': 'img-circle image-lg'}).get('src').replace('\n', '')

        # Download the logo
        img_logo = requests.get(logo, stream = True)

        # If response is ok
        if img_logo.status_code == 200:
            # Write the image as bytes
            img_logo.raw.decode_content = True
            with open(destination, 'wb') as target:
                shutil.copyfileobj(img_logo.raw, target)

    def run(self, url):
        # Create session and scrap challenge page
        self.__connect()
        soup = self.__get_box_info(url)
        return soup

# Class used for the creations of the index.md for Hugo
class wu_maker():
    def __init__(self, file, values):
        # The WU index.md file in Hugo
        self.__file = file
        # The values to fill the WU with
        self.__values = values

        # Retrieve the existing front matter from the article
        with open(self.__file, 'r') as target:
            # Remove --- and load yaml
            self.__values['front_matter'] = target.read().replace('---', '')
            self.__values['front_matter'] = yaml.safe_load(self.__values['front_matter'])

        # Get content of nmap scan
        with open(self.__values['parameters']['nmap'], 'r') as target:
            self.__values['nmap'] = target.read()

        self.__fill_front_matter()

    # Fill the front matter
    def __fill_front_matter(self):
        description_format = "**{0}** was a *{1}* difficulty {2} Machine created by {3}."
        creator_format = "[{0}]({1})"
        creator_str = []

        # Set the front matter author as the one supplied as argument in argparse
        self.__values['front_matter']['author'] = self.__values['parameters']['author'].capitalize()

        # Add each creator of the box for the description
        for creator in self.__values['box']['creators']:
            creator_str.append(creator_format.format(creator['name'], creator['url']))

        creator_str = " & ".join(creator_str)

        # Create the description text
        self.__values['front_matter']['description'] = description_format.format(self.__values['box']['box_name'].capitalize(), self.__values['box']['difficulty'].lower(), self.__values['box']['os'].capitalize(), creator_str)

        # Create HTB keywords
        self.__values['front_matter']['keywords'] = ["Hack The Box", self.__values['box']['box_name'].capitalize(), ""]
        # All HTB challenges are in Pentest
        self.__values['front_matter']["categories"] = 'Pentest'
        # Create HTB tags
        self.__values['front_matter']['tags'] = ["HTB", self.__values['box']['os'].capitalize(), ""]
        # Fill logo location
        self.__values['front_matter']['logo'] = "img/{}.png".format(connector.box_info['box_name'].lower())
        # All HTB have the Table of content
        self.__values['front_matter']['toc'] = True

    def run(self):
        # Fill template
        template = Environment(loader=FileSystemLoader(self.__values['parameters']['location'])).get_template('index.md.jinja')
        render_template = template.render(values=self.__values)
        # Write filled template
        with open(self.__file, 'w') as target:
            target.write(render_template)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='HTB WU maker.')
    parser.add_argument("-u", "--url", help="Challenge URL", type=str)
    parser.add_argument("-a", "--author", help="WU author", type=str)
    parser.add_argument("-n", "--nmap", help="Location of nmap scan", type=str)
    parser.add_argument("-d", "--directory", help="Location of hugo directory", type=str)
    args = parser.parse_args()

    # Initialize connection object
    connector = connection()
    # Scrap challenge page
    html = connector.run(args.url)

    # Prepare values for filling the WU
    wu_values = {'parameters': {'author': args.author, 'nmap': args.nmap, 'location': os.path.dirname(os.path.abspath(__file__)) + '/'}, 'box': connector.box_info}

    # Go to Hugo directory to create a new WU
    os.chdir(args.directory)
    # Create new WU with Hugo
    subprocess.run(['hugo', 'new', 'write-up/HTB/{}'.format(connector.box_info['box_name'].capitalize())])
    # Download the logo and put it in the img folder of the new WU
    connector.download_logo(html, connector.box_info['box_name'], 'content/write-up/HTB/{0}/img/{1}.png'.format(connector.box_info['box_name'].capitalize(), connector.box_info['box_name'].lower()))
    # Initialize wu_maker object
    wu_maker = wu_maker('content/write-up/HTB/{}/index.md'.format(connector.box_info['box_name'].capitalize()), wu_values)
    # Fill and write template
    wu_maker.run()

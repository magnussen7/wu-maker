# Hack the Box WU maker

This a small python script to make the creation of Hack The Box write-ups easier. It scrapes the challenge page to retrieve the name of the box, the creator, the environment, difficulty, logo etc... You can find an example of output at the end of this Readme.

It's combine to **Hugo** to automatically create a new content page and uses funcMyLife format for write-ups but you can modify *index.md.jinja* to make it your own.

## Summary

- [Installation](./README.md#installation)
- [Usage](./README.md#usage)
- [Example](./README.md#example)

### Installation

First of all you have to clone this repository (don't forget to add your SSH key in gitlab):

```bash
git clone git@gitlab.com:magnussen7/wu-maker.git
```

Then you have to install the python packages:

```bash
pip install -r requirements.txt
```

You can set the script as executable with:

```bash
chmod +x ./wu_maker.py
```

And you're good to go!

### Usage

The tool will use Hugo to create a new write-up and will fill it with the informations scrapped from **Hack the Box**.

To create a new write-up you have to supply the following options:
- **url**: the url of the box on HTB (for example https://www.hackthebox.eu/home/machines/profile/259)
- **author**: your name (for example *Magnussen*)
- **nmap**: The location of the nmap scan on your PC (for example /home/magnussen/htb/tabby/nmap.txt)
- **directory**: The location of the Hugo website directory on your PC (for example /home/magnussen/funcmylife)

Complete example:

```bash
./wu_maker.py -u "https://www.hackthebox.eu/home/machines/profile/259" -a Magnussen -n /home/magnussen/Documents/magnupackage/current_chall/tabby/tabby.txt -d /home/magnussen/Documents/funcMyLife/funcmysite
```

In this example, the write-up will be created in /home/magnussen/Documents/funcMyLife/funcmysite/content/HTB/tabby/.

The final step is to screen the overview of the challenge (the red part from the following screen) and put it in the *img* folder of the write-up as *description.png*.

![example](example.png)

## Example

Here's an example for **tabby** on **Hack the Box**:

The script will download the box logo and put it in img/*box_name*.png and create the following output in index.md.

```
---
title: "Tabby"
date: 2020-07-27 14:44:54+02:00
publishDate: 2020-07-27 14:44:54+02:00
author: "Magnussen"
description: "**Tabby** was an *easy* difficulty Linux Machine created by [egre55](https://www.hackthebox.eu/home/users/profile/1190)."
keywords: ['Hack The Box', 'Tabby', '']
categories: "Pentest"
tags: ['HTB', 'Linux', '']
logo: "img/tabby.png"
toc: True
draft: True
---

![Tabby](write-up/htb/tabby/img/description.png)

## TL;DR

- <>
- <>

## User.txt

### Reconnaissance

Let's start by a *Nmap* scan:

```bash
magnussen@funcMyLife:~/tabby$ nmap -sS -sV -sC -p- -vvv --min-rate 5000 --reason -oN tabby.txt 10.10.10.194
# Nmap 7.60 scan initiated Fri Jul 10 12:56:46 2020 as: nmap -sS -sV -sC -p- -vvv --min-rate 5000 --reason -oN remote.txt remote.htb
Increasing send delay for 10.10.10.180 from 0 to 5 due to 142 out of 473 dropped probes since last increase.
Increasing send delay for 10.10.10.180 from 5 to 10 due to 181 out of 601 dropped probes since last increase.
Warning: 10.10.10.180 giving up on port because retransmission cap hit (10).
Nmap scan report for remote.htb (10.10.10.180)
Host is up, received echo-reply ttl 127 (0.18s latency).
Scanned at 2020-07-10 12:56:46 CEST for 217s
Not shown: 53899 closed ports, 11621 filtered ports
Reason: 53899 resets and 11621 no-responses
PORT      STATE SERVICE       REASON          VERSION
21/tcp    open  ftp           syn-ack ttl 127 Microsoft ftpd
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst:
|_  SYST: Windows_NT
80/tcp    open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Home - Acme Widgets
111/tcp   open  rpcbind       syn-ack ttl 127 2-4 (RPC #100000)
| rpcinfo:
|   program version   port/proto  service
|   100000  2,3,4        111/tcp  rpcbind
|   100000  2,3,4        111/udp  rpcbind
|   100003  2,3         2049/udp  nfs
|   100003  2,3,4       2049/tcp  nfs
|   100005  1,2,3       2049/tcp  mountd
|   100005  1,2,3       2049/udp  mountd
|   100021  1,2,3,4     2049/tcp  nlockmgr
|   100021  1,2,3,4     2049/udp  nlockmgr
|   100024  1           2049/tcp  status
|_  100024  1           2049/udp  status
135/tcp   open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
139/tcp   open  netbios-ssn   syn-ack ttl 127 Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds? syn-ack ttl 127
2049/tcp  open  mountd        syn-ack ttl 127 1-3 (RPC #100005)
5985/tcp  open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
47001/tcp open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49665/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49666/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49678/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49679/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49680/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| p2p-conficker:
|   Checking for Conficker.C or higher...
|   Check 1 (port 45222/tcp): CLEAN (Couldn't connect)
|   Check 2 (port 57430/tcp): CLEAN (Couldn't connect)
|   Check 3 (port 33056/udp): CLEAN (Failed to receive data)
|   Check 4 (port 15893/udp): CLEAN (Timeout)
|_  0/4 checks are positive: Host is CLEAN or ports are blocked
| smb2-security-mode:
|   2.02:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2020-07-10 13:04:43
|_  start_date: 1601-01-01 00:09:21

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Jul 10 13:00:23 2020 -- 1 IP address (1 host up) scanned in 217.56 seconds

So we find <> useful services:
- <>

The website is <>

![Website](write-up/htb/tabby/img/website.png)

### <>

```bash
magnussen@funcMyLife:~/tabby$


## I AM ROOT

### <>

<>, thanks [Egre55](https://www.hackthebox.eu/home/users/profile/1190) for the box!
```
